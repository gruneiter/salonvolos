'use strict'

$("[data-fancybox]").fancybox({
  infobar : false,
	arrows : false,
  touch:false,
  beforeShow: function(){
    $("body").css({'overflow':'hidden'});
  },
  afterClose: function(){
      $("body").css({'overflow':'visible'});
  }
});
$('.home-services__label').on('click',function () {
  if (! $(this).parent().parent().hasClass('home-services__item_blurred')) {
    $(this).parent().parent().addClass('home-services__item_blurred')
  } else {
    $(this).parent().parent().removeClass('home-services__item_blurred')
  }

})

const menu = document.getElementById('header-menu');
// const contactsFixed = document.getElementById('header-contacts-fixed');
const menuNav = menu.querySelector('.header-menu__list')
const header = document.getElementById('header');
const headerMobileSwitcher = document.getElementById('header-mobile-switcher');

let fixedPoint = menu.offsetTop;

window.addEventListener('scroll', () => {
  let scrolled = window.pageYOffset;
  if (document.documentElement.clientWidth > 768) {
    if (scrolled >= fixedPoint) {
      header.style.marginBottom = `${menu.offsetHeight}px`;
      menu.classList.add('header-menu--fixed')
      // contactsFixed.classList.add('header-contacts-fixed--active')
    } else {
      menu.classList.remove('header-menu--fixed')
      header.style.marginBottom = 0;
      // contactsFixed.classList.remove('header-contacts-fixed--active')
    }
  }
})

headerMobileSwitcher.addEventListener('click', e => {
  if (document.documentElement.clientWidth <= 768) {
    menuNav.classList.toggle('header-menu__list--mobile-visible');
    menuNav.style.width = `${document.documentElement.clientWidth}px`;
    menuNav.style.minHeight = `${document.documentElement.clientHeight - header.offsetHeight}px`;
    e.currentTarget.classList.toggle('header-mobile-switcher--active');
  }
})

jQuery(function() {
  jQuery('.hair-extention-banner .countdownHolder').each(function(i, elem) {
    jQuery(elem).countdown(
    {
      timestamp: new Date(2018, 8, 1)
    });
  })
});


const callbackButton = document.querySelectorAll('.header-contacts__callback a');

for (let item of callbackButton) {
  item.addEventListener('click', e => {
    e.preventDefault();
    Comagic.openSitePhonePanel();
  } )
}

const onlineRecord = document.querySelectorAll('.prices-block__record-online');

for (let item of onlineRecord) {
  item.addEventListener('click', e => {
    e.preventDefault();
    show_online_record('248'); return false;
  } )
}
