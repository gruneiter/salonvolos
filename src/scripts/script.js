'use strict'

$("[data-fancybox]").fancybox({
  infobar : false,
	arrows : false,
  touch:false,
  beforeShow: function(){
    $("body").css({'overflow':'hidden'});
  },
  afterClose: function(){
      $("body").css({'overflow':'visible'});
  }
});
$('.home-services__label').on('click',function () {
  if (! $(this).parent().parent().hasClass('home-services__item_blurred')) {
    $(this).parent().parent().addClass('home-services__item_blurred')
  } else {
    $(this).parent().parent().removeClass('home-services__item_blurred')
  }

})

const menu = document.getElementById('header-menu');
// const contactsFixed = document.getElementById('header-contacts-fixed');
const menuNav = menu.querySelector('.header-menu__list')
const header = document.getElementById('header');
const headerMobileSwitcher = document.getElementById('header-mobile-switcher');

let fixedPoint = menu.offsetTop;

window.addEventListener('scroll', () => {
  let scrolled = window.pageYOffset;
  if (document.documentElement.clientWidth > 768) {
    if (scrolled >= fixedPoint) {
      header.style.marginBottom = `${menu.offsetHeight}px`;
      menu.classList.add('header-menu--fixed')
      // contactsFixed.classList.add('header-contacts-fixed--active')
    } else {
      menu.classList.remove('header-menu--fixed')
      header.style.marginBottom = 0;
      // contactsFixed.classList.remove('header-contacts-fixed--active')
    }
  }
})

headerMobileSwitcher.addEventListener('click', e => {
  if (document.documentElement.clientWidth <= 768) {
    menuNav.classList.toggle('header-menu__list--mobile-visible');
    menuNav.style.width = `${document.documentElement.clientWidth}px`;
    menuNav.style.minHeight = `${document.documentElement.clientHeight - header.offsetHeight}px`;
    e.currentTarget.classList.toggle('header-mobile-switcher--active');
  }
})

const callbackButton = document.querySelectorAll('.header-contacts__callback a');

for (let item of callbackButton) {
  item.addEventListener('click', e => {
    e.preventDefault();
    Comagic.openSitePhonePanel();
  } )
}

const onlineRecord = document.querySelectorAll('.prices-block__record-online');

for (let item of onlineRecord) {
  item.addEventListener('click', e => {
    e.preventDefault();
    show_online_record('248'); return false;
  } )
}



const hairExtentionHeaderButtons = document.getElementsByClassName( 'page-services__button' );
const hairExtentionSignSubheader = document.querySelector( '.hair-extention-sign__header span' );
const hairExtentionSignTypeInput = document.querySelector( '.hair-extention-sign-form__input-type input' );
const hairExtentionSignBottomButton = document.querySelector( '.page-appointment__btn a.btn' );
const hairExtentionSignOnlineButton = document.querySelector( 'a.prices-block__record-online' );

console.log(hairExtentionHeaderButtons, hairExtentionSignSubheader, hairExtentionSignTypeInput, hairExtentionSignBottomButton);

const hairExtentionButtons = Array.from(hairExtentionHeaderButtons);

if(hairExtentionSignBottomButton){
    hairExtentionButtons.push(hairExtentionSignBottomButton);
}

if(hairExtentionSignOnlineButton){
    hairExtentionButtons.push(hairExtentionSignOnlineButton);
}

for ( var item of hairExtentionButtons ) {
  item.addEventListener('click', function( event ) {
    var signWindowTypeText = this.dataset.name;
    hairExtentionSignSubheader.innerText = signWindowTypeText;
    if (signWindowTypeText !== 'наращивание') {
      hairExtentionSignTypeInput.setAttribute('value', signWindowTypeText);
    } else {
      hairExtentionSignTypeInput.setAttribute('value', 'тип не указан');
    }
  });
}
